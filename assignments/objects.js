const testObject = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
}; 

function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  let arr = [];
  for (let key in obj) {
    arr.push(key);
  }
  return arr;
}


function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values
  let arr = [];
  for (let key in obj) {
    arr.push(obj[key]);
  }
  return arr; 
}

function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
  // let arr = [];
  for (let key in obj) {
    let transformation = cb(obj[key]);
    obj[key] = transformation;
  }
  return obj;
}

function cb(element) {
  return (element *= 5); //logic
}


function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs

  let arr2 = [];
  for (let key in obj) {
    let arr1 = [];
    arr1.push(key, obj[key]);
    arr2.push(arr1);
  }
  return arr2;
}




function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
  let newObj = {};
  for (let item in obj) {
    newObj[obj[item]] = item;
  }
  return newObj;
}

function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults
  let arrDefaults = [];
  for (let key in defaultProps) {
    arrDefaults.push(key);
  }
  for(let i=0; i<arrDefaults.length;i++) {
    if(!obj[arrDefaults[i]]) {
      obj[arrDefaults[i]] = defaultProps[arrDefaults[i]];
    }
  }
  return obj;
}