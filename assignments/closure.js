function counterFactory() {
  //  Return an object that has two methods called `increment` and `decrement`.
  //  increment` should increment a counter variable in closure scope and return it.
  //  `decrement` should decrement the counter variable and return it.

  var counter = 0;

  function counterGenerator(val) {
    counter += val;
    return counter;
  }

  return {
    increment: function () {
      return counterGenerator(1);
    },
    decrement: function () {
      return counterGenerator(-1);
    },
  };
}

function limitFunctionCallCount(cb, n) {
  // Should return a function that invokes `cb`.
  // The returned function should only allow `cb` to be invoked `n` times.
  // Returning null is acceptable if cb can't be returned
  return function checker() {
    if (n === null) {
      return null;
    } else {
      for (let i = 1; i <= n; i++) {
        let result = cb();
      }
    }
  };
}

function cb_1() {
  return "Hello World!";
}

function cacheFunction(cb) {
  // Should return a function that invokes `cb`.
  // A cache (object) should be kept in closure scope.
  // The cache should keep track of all arguments have been used to invoke this function.
  // If the returned function is invoked with arguments that it has already seen
  // then it should return the cached result and not invoke `cb` again.
  // `cb` should only ever be invoked once for a given set of arguments.
  let cache = {};

  return function (...args) {
    let argsString = JSON.stringify(args);
    if (cache.hasOwnProperty(argsString)) {
      return cache[argsString];
    } else {
      cache[argsString] = cb(argsString);
      return cache[argsString];
    }
  };
}

function cb1(input) {
  return JSON.parse(input);
}